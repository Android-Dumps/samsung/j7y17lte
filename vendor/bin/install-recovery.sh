#!/vendor/bin/sh
if ! applypatch --check EMMC:/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY$(getprop ro.boot.slot_suffix):29523984:0826d08b66385836dba497e417f47967dded9692; then
  applypatch --bonus /vendor/etc/recovery-resource.dat \
          --patch /vendor/recovery-from-boot.p \
          --source EMMC:/dev/block/platform/13540000.dwmmc0/by-name/BOOT$(getprop ro.boot.slot_suffix):21235728:1481a0a955ad77883cd8d672af4a9e18f892eef0 \
          --target EMMC:/dev/block/platform/13540000.dwmmc0/by-name/RECOVERY$(getprop ro.boot.slot_suffix):29523984:0826d08b66385836dba497e417f47967dded9692 && \
      log -t recovery "Installing new recovery image: succeeded" || \
      log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
