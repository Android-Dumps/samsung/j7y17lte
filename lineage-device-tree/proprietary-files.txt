# Unpinned blobs from lineage_j7y17lte-eng 12 SQ3A.220705.004 eng.root.20221129.163020 test-keys

# Audio
vendor/bin/hw/android.hardware.audio.service
vendor/etc/init/android.hardware.audio.service.rc
vendor/lib/hw/android.hardware.audio.effect@7.0-impl.so
vendor/lib/hw/android.hardware.audio@7.0-impl.so
vendor/lib/hw/audio.primary.default.so
vendor/lib/hw/audio.primary.universal7870.so
vendor/lib/hw/audio.r_submix.default.so
vendor/lib/hw/audio.usb.default.so
vendor/lib/libalsautils.so
vendor/lib/libfloatingfeature.so
vendor/lib/libmediautils_vendor.so
vendor/lib/libtfa98xx.so
vendor/lib/libvndsecril-client.so
vendor/lib64/hw/android.hardware.audio.effect@7.0-impl.so
vendor/lib64/hw/android.hardware.audio@7.0-impl.so
vendor/lib64/hw/audio.primary.default.so
vendor/lib64/hw/audio.r_submix.default.so
vendor/lib64/hw/audio.usb.default.so
vendor/lib64/libalsautils.so
vendor/lib64/libfloatingfeature.so
vendor/lib64/libmediautils_vendor.so
vendor/lib64/libvndsecril-client.so

# Audio (FX modules)
vendor/lib/soundfx/libdynproc.so
vendor/lib/soundfx/libhapticgenerator.so
vendor/lib/libvibrator.so
vendor/lib64/soundfx/libdynproc.so
vendor/lib64/soundfx/libhapticgenerator.so
vendor/lib64/libvibrator.so

# Audio configs
etc/audio_effects.conf
vendor/etc/a2dp_audio_policy_configuration.xml
vendor/etc/a2dp_in_audio_policy_configuration.xml
vendor/etc/audio_policy_volumes.xml
vendor/etc/bluetooth_audio_policy_configuration.xml
vendor/etc/default_volume_tables.xml
vendor/etc/mixer_paths_0.xml
vendor/etc/r_submix_audio_policy_configuration.xml
vendor/etc/usb_audio_policy_configuration.xml

# Bluetooth
vendor/bin/hw/android.hardware.bluetooth@1.0-service
vendor/etc/init/android.hardware.bluetooth@1.0-service.rc

# Bluetooth (A2DP)
vendor/lib/hw/android.hardware.bluetooth.audio@2.0-impl.so
vendor/lib/hw/audio.bluetooth.default.so
vendor/lib/libbluetooth_audio_session.so
vendor/lib64/hw/android.hardware.bluetooth.audio@2.0-impl.so
vendor/lib64/hw/audio.bluetooth.default.so
vendor/lib64/libbluetooth_audio_session.so

# Broadcast radio
vendor/lib/hw/android.hardware.broadcastradio@1.0-impl.so
vendor/lib64/hw/android.hardware.broadcastradio@1.0-impl.so

# Camera
vendor/bin/hw/android.hardware.camera.provider@2.5-service
vendor/etc/init/android.hardware.camera.provider@2.5-service.rc
vendor/lib/hw/android.hardware.camera.provider@2.4-impl.so
vendor/lib/hw/camera.exynos5.so
vendor/lib/hw/camera.universal7870.so
vendor/lib/camera.device@1.0-impl.so
vendor/lib/camera.device@3.2-impl.so
vendor/lib/camera.device@3.3-impl.so
vendor/lib/camera.device@3.4-external-impl.so
vendor/lib/camera.device@3.4-impl.so
vendor/lib/camera.device@3.5-external-impl.so
vendor/lib/camera.device@3.5-impl.so
vendor/lib/camera.device@3.6-external-impl.so
vendor/lib/libGrallocWrapper.so
vendor/lib/libcsc.so
vendor/lib/libexynoscamera_shim.so
vendor/lib/libexynosgscaler.so
vendor/lib/libexynosscaler.so
vendor/lib/libexynosutils.so
vendor/lib/libexynosv4l2.so
vendor/lib/libhwjpeg.so
vendor/lib/libion_exynos.so
vendor/lib64/hw/android.hardware.camera.provider@2.4-impl.so
vendor/lib64/hw/camera.universal7870.so
vendor/lib64/camera.device@1.0-impl.so
vendor/lib64/camera.device@3.2-impl.so
vendor/lib64/camera.device@3.3-impl.so
vendor/lib64/camera.device@3.4-external-impl.so
vendor/lib64/camera.device@3.4-impl.so
vendor/lib64/camera.device@3.5-external-impl.so
vendor/lib64/camera.device@3.5-impl.so
vendor/lib64/camera.device@3.6-external-impl.so
vendor/lib64/libGrallocWrapper.so
vendor/lib64/libcsc.so
vendor/lib64/libexynoscamera_shim.so
vendor/lib64/libexynosgscaler.so
vendor/lib64/libexynosscaler.so
vendor/lib64/libexynosutils.so
vendor/lib64/libexynosv4l2.so
vendor/lib64/libhwjpeg.so
vendor/lib64/libion_exynos.so

# CAS
vendor/bin/hw/android.hardware.cas@1.2-service
vendor/etc/init/android.hardware.cas@1.2-service.rc
vendor/etc/vintf/manifest/android.hardware.cas@1.2-service.xml

# Configstore
vendor/bin/hw/android.hardware.configstore@1.1-service
vendor/etc/init/android.hardware.configstore@1.1-service.rc
vendor/lib64/libhwminijail.so

# Display
vendor/bin/hw/android.hardware.graphics.allocator@2.0-service
vendor/bin/hw/android.hardware.graphics.composer@2.2-service
vendor/bin/hw/android.hardware.memtrack@1.0-service
vendor/etc/init/android.hardware.graphics.allocator@2.0-service.rc
vendor/etc/init/android.hardware.graphics.composer@2.2-service.rc
vendor/etc/init/android.hardware.memtrack@1.0-service.rc
vendor/lib/egl/libGLES_mali.so
vendor/lib/hw/android.hardware.graphics.allocator@2.0-impl.so
vendor/lib/hw/android.hardware.graphics.mapper@2.0-impl.so
vendor/lib/hw/android.hardware.memtrack@1.0-impl.so
vendor/lib/hw/gralloc.default.so
vendor/lib/hw/gralloc.exynos5.so
vendor/lib/hw/hwcomposer.exynos5.so
vendor/lib/hw/memtrack.exynos5.so
vendor/lib/hw/vulkan.exynos5.so
vendor/lib/libExynosHWCService.so
vendor/lib/libexynosdisplay.so
vendor/lib/libhdmi.so
vendor/lib/libhwc2onfbadapter.so
vendor/lib/libhwcutils.so
vendor/lib/libmpp.so
vendor/lib64/egl/libGLES_mali.so
vendor/lib64/hw/android.hardware.graphics.allocator@2.0-impl.so
vendor/lib64/hw/android.hardware.graphics.mapper@2.0-impl.so
vendor/lib64/hw/android.hardware.memtrack@1.0-impl.so
vendor/lib64/hw/gralloc.default.so
vendor/lib64/hw/gralloc.exynos5.so
vendor/lib64/hw/hwcomposer.exynos5.so
vendor/lib64/hw/memtrack.exynos5.so
vendor/lib64/hw/vulkan.exynos5.so
vendor/lib64/libExynosHWCService.so
vendor/lib64/libexynosdisplay.so
vendor/lib64/libhdmi.so
vendor/lib64/libhwc2onfbadapter.so
vendor/lib64/libhwcutils.so
vendor/lib64/libmpp.so

# DRM
vendor/bin/hw/android.hardware.drm@1.4-service.clearkey
vendor/etc/init/android.hardware.drm@1.2-service.widevine.rc
vendor/etc/init/android.hardware.drm@1.4-service.clearkey.rc
vendor/etc/vintf/manifest/manifest_android.hardware.drm@1.4-service.clearkey.xml
vendor/lib/hw/android.hardware.drm@1.0-impl.so
vendor/lib/mediacas/libclearkeycasplugin.so
vendor/lib/mediadrm/libdrmclearkeyplugin.so
vendor/lib/mediadrm/libwvdrmengine.so
vendor/lib/libMcClient.so
vendor/lib/liboemcrypto.so
vendor/lib64/hw/android.hardware.drm@1.0-impl.so
vendor/lib64/mediacas/libclearkeycasplugin.so
vendor/lib64/mediadrm/libdrmclearkeyplugin.so

# Gatekeeper
vendor/bin/hw/android.hardware.gatekeeper@1.0-service.software
vendor/etc/init/android.hardware.gatekeeper@1.0-service.software.rc
vendor/etc/vintf/manifest/android.hardware.gatekeeper@1.0-service.software.xml

# GNSS
vendor/lib64/hw/android.hardware.gnss@2.0-impl.so

# Health
vendor/bin/hw/android.hardware.health@2.0-service
vendor/etc/init/android.hardware.health@2.0-service.rc

# Keymaster
vendor/bin/hw/android.hardware.keymaster@4.0-service
vendor/etc/init/android.hardware.keymaster@4.0-service.rc
vendor/lib64/libkeymaster4.so

# Light
vendor/bin/hw/android.hardware.light-service.samsung
vendor/etc/init/android.hardware.light-service.samsung.rc
vendor/etc/vintf/manifest/android.hardware.light-service.samsung.xml

# Local time
vendor/lib/hw/local_time.default.so
vendor/lib64/hw/local_time.default.so

# Media (OMX)
apex/com.android.media.swcodec/lib64/android.hardware.media.bufferpool@2.0.so
apex/com.android.media.swcodec/lib64/android.hidl.token@1.0-utils.so
apex/com.android.vndk.current/lib/android.hardware.media.bufferpool@2.0.so
apex/com.android.vndk.current/lib/android.hidl.token@1.0-utils.so
apex/com.android.vndk.current/lib64/android.hardware.media.bufferpool@2.0.so
apex/com.android.vndk.current/lib64/android.hidl.token@1.0-utils.so
etc/seccomp_policy/mediacodec.policy
lib/libmedia_codeclist.so
lib/libstagefright_bufferpool@2.0.1.so
lib/libstagefright_codecbase.so
lib/libstagefright_framecapture_utils.so
lib/libstagefright_shim.so
lib/libstagefright_wfd.so
lib64/libmedia_codeclist.so
lib64/libstagefright_bufferpool@2.0.1.so
lib64/libstagefright_bufferqueue_helper_novndk.so
lib64/libstagefright_codecbase.so
lib64/libstagefright_framecapture_utils.so
lib64/libstagefright_shim.so
vendor/bin/hw/android.hardware.media.omx@1.0-service
vendor/etc/init/android.hardware.media.omx@1.0-service.rc
vendor/etc/seccomp_policy/mediacodec.policy
vendor/lib/libstagefright_softomx_plugin.so
vendor/lib/libstagefrighthw.so
vendor/lib64/libstagefrighthw.so

# Media configs
vendor/etc/media_codecs.xml
vendor/etc/media_codecs_google_audio.xml
vendor/etc/media_codecs_google_telephony.xml
vendor/etc/media_codecs_google_video_le.xml
vendor/etc/media_codecs_performance.xml
vendor/etc/media_profiles_V1_0.xml

# NFC configs
etc/libnfc-nci.conf

# Power
vendor/bin/hw/android.hardware.power@1.0-service.exynos7870
vendor/etc/init/android.hardware.power@1.0-service.exynos7870.rc
vendor/lib/hw/power.default.so
vendor/lib64/hw/power.default.so
vendor/lib64/vendor.customelineage.power@1.0.so

# Sensors
vendor/bin/hw/android.hardware.sensors@1.0-service
vendor/etc/init/android.hardware.sensors@1.0-service.rc
vendor/lib/hw/android.hardware.sensors@1.0-impl.so
vendor/lib64/hw/android.hardware.sensors@1.0-impl.so

# Soundtrigger
vendor/lib/hw/android.hardware.soundtrigger@2.3-impl.so
vendor/lib64/hw/android.hardware.soundtrigger@2.3-impl.so

# Thermal
vendor/bin/hw/android.hardware.thermal@2.0-service.samsung
vendor/etc/init/android.hardware.thermal@2.0-service.samsung.rc
vendor/etc/vintf/manifest/android.hardware.thermal@2.0-service.samsung.xml
vendor/lib/hw/thermal.universal7870.so
vendor/lib64/hw/thermal.universal7870.so

# USB
vendor/bin/hw/android.hardware.usb@1.3-service.samsung
vendor/etc/init/android.hardware.usb@1.3-service.samsung.rc
vendor/etc/vintf/manifest/android.hardware.usb@1.3-service.samsung.xml

# Vibrator
vendor/bin/hw/android.hardware.vibrator@1.0-service
vendor/etc/init/android.hardware.vibrator@1.0-service.rc
vendor/lib/hw/android.hardware.vibrator@1.0-impl.so
vendor/lib/hw/vibrator.default.so
vendor/lib64/hw/android.hardware.vibrator@1.0-impl.so
vendor/lib64/hw/vibrator.default.so

# Wi-Fi
vendor/bin/hw/android.hardware.wifi@1.0-service
vendor/bin/hw/hostapd
vendor/bin/hw/wpa_supplicant
vendor/etc/init/android.hardware.wifi@1.0-service.rc
vendor/etc/vintf/manifest/android.hardware.wifi.hostapd.xml
vendor/etc/vintf/manifest/android.hardware.wifi.supplicant.xml
vendor/etc/vintf/manifest/android.hardware.wifi@1.0-service.xml
vendor/lib/libwifi-hal.so
vendor/lib64/libkeystore-engine-wifi-hidl.so
vendor/lib64/libkeystore-wifi-hidl.so
vendor/lib64/libwifi-hal.so

# Wi-Fi configs
vendor/etc/wifi/p2p_supplicant_overlay.conf
vendor/etc/wifi/wpa_supplicant.conf
vendor/etc/wifi/wpa_supplicant_overlay.conf

# Miscellaneous
vendor/app/mcRegistry/00060308060501020000000000000000.tlbin
vendor/app/mcRegistry/07010000000000000000000000000000.tlbin
vendor/app/mcRegistry/07060000000000000000000000000000.tlbin
vendor/app/mcRegistry/08130000000000000000000000000000.tlbin
vendor/app/mcRegistry/FFFFFFFF000000000000000000000001.drbin
vendor/app/mcRegistry/ffffffff000000000000000000000005.tlbin
vendor/app/mcRegistry/ffffffff00000000000000000000000a.tlbin
vendor/app/mcRegistry/ffffffff00000000000000000000000b.tlbin
vendor/app/mcRegistry/ffffffff00000000000000000000000c.tlbin
vendor/app/mcRegistry/ffffffff00000000000000000000000d.tlbin
vendor/app/mcRegistry/ffffffff00000000000000000000000f.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000012.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000013.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000016.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000017.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000019.tlbin
vendor/app/mcRegistry/ffffffff00000000000000000000002e.tlbin
vendor/app/mcRegistry/ffffffff00000000000000000000002f.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000030.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000038.tlbin
vendor/app/mcRegistry/ffffffff00000000000000000000003e.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000041.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000045.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000059.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000060.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000073.tlbin
vendor/app/mcRegistry/ffffffff000000000000000000000077.tlbin
vendor/app/mcRegistry/ffffffffd0000000000000000000000a.tlbin
vendor/app/mcRegistry/ffffffffd0000000000000000000000e.tlbin
vendor/app/mcRegistry/ffffffffd00000000000000000000014.tlbin
vendor/app/mcRegistry/ffffffffd00000000000000000000016.tlbin
vendor/app/mcRegistry/ffffffffd00000000000000000000062.tlbin
vendor/bin/hw/gpsd
vendor/bin/hw/rild
vendor/bin/hw/vendor.lineage.fastcharge@1.0-service.samsung
vendor/bin/hw/vendor.lineage.livedisplay@2.0-service.samsung-exynos
vendor/bin/hw/vendor.samsung.hardware.gnss@2.0-service
vendor/bin/[
vendor/bin/applypatch
vendor/bin/boringssl_self_test32
vendor/bin/boringssl_self_test64
vendor/bin/cbd
vendor/bin/chattr
vendor/bin/devmem
vendor/bin/dumpsys
vendor/bin/fsync
vendor/bin/getconf
vendor/bin/i2cdetect
vendor/bin/i2cdump
vendor/bin/i2cget
vendor/bin/i2cset
vendor/bin/iconv
vendor/bin/install
vendor/bin/lsattr
vendor/bin/macloader
vendor/bin/mcDriverDaemon
vendor/bin/nc
vendor/bin/netcat
vendor/bin/nproc
vendor/bin/nsenter
vendor/bin/readelf
vendor/bin/rtcwake
vendor/bin/secril_config_svc
vendor/bin/sswap
vendor/bin/test
vendor/bin/unlink
vendor/bin/unshare
vendor/bin/uuidgen
vendor/bin/watch
vendor/bin/wifiloader
vendor/etc/bluetooth/bt_vendor.conf
vendor/etc/gnss/ca.pem
vendor/etc/gnss/gps.cfg
vendor/etc/init/boringssl_self_test.rc
vendor/etc/init/hostapd.android.rc
vendor/etc/init/init.baseband.rc
vendor/etc/init/init.samsungexynos7870.rc
vendor/etc/init/init.samsungexynos7870.usb.rc
vendor/etc/init/init.vendor.rilchip.rc
vendor/etc/init/init.vendor.rilcommon.rc
vendor/etc/init/init.wifi.rc
vendor/etc/init/init.wifi_device.rc
vendor/etc/init/mobicore.rc
vendor/etc/init/vendor.lineage.fastcharge@1.0-service.samsung.rc
vendor/etc/init/vendor.lineage.livedisplay@2.0-service.samsung-exynos.rc
vendor/etc/init/vendor.samsung.hardware.gnss@2.0-service.rc
vendor/etc/init/vendor_flash_recovery.rc
vendor/etc/init/vndservicemanager.rc
vendor/etc/seccomp_policy/configstore@1.1.policy
vendor/etc/seccomp_policy/mediaextractor.policy
vendor/etc/vintf/manifest/vendor.lineage.fastcharge@1.0-service.samsung.xml
vendor/etc/Tfa9896.cnt
vendor/etc/external_camera_config.xml
vendor/etc/fstab.samsungexynos7870
vendor/etc/mkshrc
vendor/etc/plmn_delta.bin
vendor/etc/plmn_delta_attaio.bin
vendor/etc/plmn_delta_hktw.bin
vendor/etc/plmn_delta_usacdma.bin
vendor/etc/plmn_delta_usagsm.bin
vendor/etc/plmn_se13.bin
vendor/etc/recovery-resource.dat
vendor/firmware/mfc_fw.bin
vendor/lib/omx/libOMX.Exynos.AVC.Decoder.so
vendor/lib/omx/libOMX.Exynos.AVC.Encoder.so
vendor/lib/omx/libOMX.Exynos.HEVC.Decoder.so
vendor/lib/omx/libOMX.Exynos.HEVC.Encoder.so
vendor/lib/omx/libOMX.Exynos.MPEG4.Decoder.so
vendor/lib/omx/libOMX.Exynos.MPEG4.Encoder.so
vendor/lib/omx/libOMX.Exynos.VP8.Decoder.so
vendor/lib/omx/libOMX.Exynos.VP8.Encoder.so
vendor/lib/omx/libOMX.Exynos.VP9.Decoder.so
vendor/lib/omx/libOMX.Exynos.WMV.Decoder.so
vendor/lib/libExynosOMX_Core.so
vendor/lib/libExynosOMX_Resourcemanager.so
vendor/lib/libMcRegistry.so
vendor/lib/libOpenCL.so
vendor/lib/libOpenCL.so.1
vendor/lib/libOpenCL.so.1.1
vendor/lib/libbauthtzcommon_shim.so
vendor/lib/libbt-vendor.so
vendor/lib/libcamera_client_symboles_shim.so
vendor/lib/libcutils_shim.so
vendor/lib/libfimg.so
vendor/lib/libsecnativefeature.so
vendor/lib/libsensorlistener.so
vendor/lib/libsensorndkbridge.so
vendor/lib/libstainkiller.so
vendor/lib/libtinyxml.so
vendor/lib/libuniplugin.so
vendor/lib/libvpx.so
vendor/lib/libwpa_client.so
vendor/lib/libwrappergps.so
vendor/lib/libwvhidl.so
vendor/lib/vendor.samsung.hardware.gnss@2.0.so
vendor/lib64/hw/vendor.samsung.hardware.gnss@2.0-impl.so
vendor/lib64/omx/libOMX.Exynos.AVC.Decoder.so
vendor/lib64/omx/libOMX.Exynos.AVC.Encoder.so
vendor/lib64/omx/libOMX.Exynos.HEVC.Decoder.so
vendor/lib64/omx/libOMX.Exynos.HEVC.Encoder.so
vendor/lib64/omx/libOMX.Exynos.MPEG4.Decoder.so
vendor/lib64/omx/libOMX.Exynos.MPEG4.Encoder.so
vendor/lib64/omx/libOMX.Exynos.VP8.Decoder.so
vendor/lib64/omx/libOMX.Exynos.VP8.Encoder.so
vendor/lib64/omx/libOMX.Exynos.VP9.Decoder.so
vendor/lib64/omx/libOMX.Exynos.WMV.Decoder.so
vendor/lib64/libExynosOMX_Core.so
vendor/lib64/libExynosOMX_Resourcemanager.so
vendor/lib64/libOpenCL.so
vendor/lib64/libOpenCL.so.1
vendor/lib64/libOpenCL.so.1.1
vendor/lib64/libbauthtzcommon_shim.so
vendor/lib64/libbinderdebug.so
vendor/lib64/libbt-vendor.so
vendor/lib64/libcamera_client_symboles_shim.so
vendor/lib64/libcppbor_external.so
vendor/lib64/libcppcose_rkp.so
vendor/lib64/libcutils_shim.so
vendor/lib64/libfimg.so
vendor/lib64/libsec-ril-dsds.so
vendor/lib64/libsec-ril.so
vendor/lib64/libsecnativefeature.so
vendor/lib64/libsensorlistener.so
vendor/lib64/libsensorndkbridge.so
vendor/lib64/libsoft_attestation_cert.so
vendor/lib64/libtinyxml.so
vendor/lib64/libuniplugin.so
vendor/lib64/libvkmanager_vendor.so
vendor/lib64/libwpa_client.so
vendor/lib64/libwrappergps.so
vendor/lib64/libz_stable.so
vendor/lib64/vendor.lineage.fastcharge@1.0.so
vendor/lib64/vendor.lineage.livedisplay@2.0.so
vendor/lib64/vendor.samsung.hardware.gnss@2.0.so
vendor/lib64/vendor.samsung.hardware.radio.bridge@2.0.so
vendor/lib64/vendor.samsung.hardware.radio.channel@2.0.so
vendor/lib64/vendor.samsung.hardware.radio@2.0.so
vendor/lib64/vendor.samsung.hardware.radio@2.1.so
vendor/usr/idc/AVRCP.idc
vendor/usr/idc/qwerty.idc
vendor/usr/idc/qwerty2.idc
vendor/usr/keylayout/gpio-keys.kl
vendor/usr/keylayout/sec_touchkey.kl
vendor/usr/keylayout/sec_touchscreen.kl
