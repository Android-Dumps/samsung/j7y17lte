## lineage_j7y17lte-eng 12 SQ3A.220705.004 eng.root.20221129.163020 test-keys
- Manufacturer: samsung
- Platform: exynos5
- Codename: j7y17lte
- Brand: samsung
- Flavor: lineage_j7y17lte-eng
- Release Version: 12
- Kernel Version: 3.18.140
- Id: SQ3A.220705.004
- Incremental: eng.root.20221129.163020
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: samsung/lineage_j7y17lte/j7y17lte:12/SQ3A.220705.004/root11291630:eng/test-keys
- OTA version: 
- Branch: lineage_j7y17lte-eng-12-SQ3A.220705.004-eng.root.20221129.163020-test-keys
- Repo: samsung/j7y17lte
